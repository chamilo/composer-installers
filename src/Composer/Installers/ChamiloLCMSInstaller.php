<?php
namespace Composer\Installers;

class ChamiloLCMSInstaller extends BaseInstaller
{
    protected $locations = array(
        'core' => 'core/{$name}/',
        'application' => 'application/{$name}/',
        'weblcms-tool' => 'application/weblcms/tool/{$name}/',
        'content-object'  => 'repository/content_object/{$name}/',
        'external-repository'  => 'common/extensions/external_repository_manager/implementation/{$name}/',
    );
}
